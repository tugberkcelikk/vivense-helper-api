package com.vivense.vivensehelperapi.file;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileTest {

    @Test
    void file() throws IOException {
        byte[] content = Files.readAllBytes(Paths.get("baba.txt"));

        String contentAsStr = String.valueOf(content);

        Assertions.assertThat(contentAsStr.equalsIgnoreCase("baba"));

    }

    @Test
    void exel() throws IOException {
        byte[] contentOfExel = Files.readAllBytes(Paths.get("Book.xlsx"));

        File file = new File("Boosk.dxlsx");
        Assertions.assertThat(file.isAbsolute());

        // String contentAsStr = String.valueOf(content);
        FileInputStream file1 = new FileInputStream(new File("Book.xlsx"));

        Workbook workbook = new XSSFWorkbook(file1);


        Assertions.assertThat(workbook != null);
    }
}
