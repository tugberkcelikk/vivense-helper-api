package com.vivense.vivensehelperapi;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class RawExcelParser {

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response;

    @Test
    void parser() throws IOException {


        Workbook workbook = new XSSFWorkbook(request.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);

        // each row cell set if business occured
        for (Row row : sheet) {
            // manipulateRow(row);
            String id = row.getCell(0).toString();
            String name = row.getCell(1).toString();
            String surname = row.getCell(2).toString();
            String evli = row.getCell(3).toString();
            String operationType = row.getCell(19).toString();

            if (operationType.equalsIgnoreCase("Üretici kargo/nakliyat")) {

                row.getCell(3).setCellValue("false");
            }
            else {

            }

            String s = row.getCell(3).toString();
        }

        // http response sending
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            workbook.write(bos); // this is modified workbook.
        } finally {
            bos.close();
        }

        byte[] modifiedExcel = bos.toByteArray();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "filename=\"THE FILE NAME\"");
        response.setContentLength(modifiedExcel.length);

        OutputStream os = response.getOutputStream();

        try {
            os.write(modifiedExcel , 0, modifiedExcel.length);
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            os.close();
        }
        System.out.println("Sendin excel to client as a http response please download the response");
    }
}
