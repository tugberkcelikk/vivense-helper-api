package com.vivense.vivensehelperapi;

import com.vivense.vivensehelperapi.api.ExcelController;
import com.vivense.vivensehelperapi.service.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ExcelController.class)
public class ExcelControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExcelService excelService;

    @MockBean
    HttpServletRequest request;

    // @Test
    public void greetingShouldReturnMessageFromService() throws Exception {

        Files.readAllBytes(Paths.get("baba.txt"));
        byte[] body = new byte[100000];
        when(request.getInputStream().read(body)).thenReturn(1);
        // when(excelService.getExcelFromHttpRequestBody(request)).thenReturn(null);

        this.mockMvc.perform(get("/excel")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().bytes(null));
    }


}
