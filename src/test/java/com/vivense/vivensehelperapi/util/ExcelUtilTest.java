package com.vivense.vivensehelperapi.util;

import org.apache.poi.ss.usermodel.Sheet;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.mockito.Mockito.when;

class ExcelUtilTest {

    @MockBean
    HttpServletRequest request;

    @Mock
    ExcelUtil excelUtil;

    void name() throws IOException {
        when(request.getInputStream()).thenReturn(null);
        Sheet sheet = ExcelUtil.sheetFromWorkbookByPosition(request, 0);
        Assertions.assertThat(sheet != null);
    }
}