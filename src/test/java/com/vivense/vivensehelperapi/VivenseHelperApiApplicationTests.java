package com.vivense.vivensehelperapi;

import com.vivense.vivensehelperapi.api.HomeController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class VivenseHelperApiApplicationTests {

    @Autowired
    HomeController homeController;

    @Test
    void contextLoads() {
    }

}
