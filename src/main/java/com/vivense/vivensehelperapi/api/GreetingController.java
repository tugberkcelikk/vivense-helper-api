package com.vivense.vivensehelperapi.api;

import com.vivense.vivensehelperapi.service.GreetingService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@ApiIgnore
@Api(hidden = true)
public class GreetingController {

    private final GreetingService greetingService;

    public GreetingController(GreetingService service) {
        this.greetingService = service;
    }

    @GetMapping("greeting")
    public @ResponseBody
    String greeting() {
        return greetingService.greet();
    }

}
