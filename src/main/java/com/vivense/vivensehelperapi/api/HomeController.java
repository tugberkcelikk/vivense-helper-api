package com.vivense.vivensehelperapi.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@ApiIgnore
@Api(hidden = true)
public class HomeController {

    @GetMapping("")
    public void home(HttpServletResponse response) throws IOException {
        response.sendRedirect("swagger-ui/index.html");
    }


}
