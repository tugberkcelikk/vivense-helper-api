package com.vivense.vivensehelperapi.api;

import com.vivense.vivensehelperapi.service.ExcelService;
import com.vivense.vivensehelperapi.service.GreetingService;
import com.vivense.vivensehelperapi.util.ExcelUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("excel")
public class ExcelController {
    private final ExcelService excelService;

    public ExcelController(ExcelService excelService) {
        this.excelService = excelService;
    }

    @ApiOperation(value = "This method is used to get the current date.", hidden = true)
    @PostMapping("file")
    public String uploadFile(@RequestPart(required = true) MultipartFile file) throws IOException {

        String contentOfFile = new String(file.getBytes());

        System.out.println(file.getName());
        System.out.println("slam");

        return "hello";
    }

    @PostMapping("v2/indirim-orani/{indirimOrani}")
    public void excelv2(HttpServletRequest request, HttpServletResponse response, @PathVariable int indirimOrani, @RequestPart(required = true) MultipartFile file) throws IOException { // 10

        Sheet sheet = ExcelUtil.sheetFromWorkbookByPositionMultiPartFile(file, 0);

        // each row cell set if business occured
        for (Row row : sheet) {
            // manipulateRow(row);
            String id = row.getCell(0).toString();
            if (id.equals("ID")) continue; // header hack
            String name = row.getCell(1).toString();
            double alisFiyati = row.getCell(7).getNumericCellValue();
            double kampanyaliAlisFiyati = (alisFiyati * (100 - indirimOrani)) / 100.;

            Cell kampanyaAlisFiyati = row.createCell(14);

            if (kampanyaAlisFiyati == null) {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }
            else {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }

            String evli = row.getCell(3).toString();
            String operationType = row.getCell(19).toString();

            // marjing
            if (operationType.equalsIgnoreCase("Üretici kargo/nakliyat") || operationType.equalsIgnoreCase("Üretici kargo")) {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / 0.95);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    row.getCell(15).setCellValue(kampanyaliSatisFiyati);
                }
                else {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
            }
            else {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / 0.82);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
                else {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
            }
        }

        // http response sending
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            sheet.getWorkbook().write(bos); // this is modified workbook.
        } finally {
            bos.close();
        }

        byte[] modifiedExcel = bos.toByteArray();

        response.setHeader("Content-Disposition", "attachment;filename=myExcel.xls");
        response.setContentType("application/vnd.ms-excel"); // todo: produced
        response.setContentLength(modifiedExcel.length);

        OutputStream os = response.getOutputStream();

        try {
            os.write(modifiedExcel , 0, modifiedExcel.length);
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            os.close();
        }
        System.out.println("Sending excel to client as a http response please download the response");
    }


    @ApiOperation(value = "This method is used to get the current date.", hidden = true)
    // from postman logic
    @PostMapping("indirim-orani/{indirimOrani}")
    public void excel(HttpServletRequest request, HttpServletResponse response, @PathVariable int indirimOrani) throws IOException { // 10

        Sheet sheet = ExcelUtil.sheetFromWorkbookByPosition(request, 0);

        // each row cell set if business occured
        for (Row row : sheet) {
            // manipulateRow(row);
            String id = row.getCell(0).toString();
            if (id.equals("ID")) continue; // header hack
            String name = row.getCell(1).toString();
            double alisFiyati = row.getCell(7).getNumericCellValue();
            double kampanyaliAlisFiyati = (alisFiyati * (100 - indirimOrani)) / 100.;

            Cell kampanyaAlisFiyati = row.createCell(14);

            if (kampanyaAlisFiyati == null) {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }
            else {
                kampanyaAlisFiyati.setCellValue(kampanyaliAlisFiyati);
            }

            String evli = row.getCell(3).toString();
            String operationType = row.getCell(19).toString();

            // marjing
            if (operationType.equalsIgnoreCase("Üretici kargo/nakliyat") || operationType.equalsIgnoreCase("Üretici kargo")) {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / 0.95);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    row.getCell(15).setCellValue(kampanyaliSatisFiyati);
                }
                else {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
            }
            else {
                double kampanyaAlisFiyatiCellValue = kampanyaAlisFiyati.getNumericCellValue();

                double kampanyaliSatisFiyati = Math.ceil(kampanyaAlisFiyatiCellValue / 0.82);

                Cell kampanyaliSatisFiyatiCell = row.createCell(15);

                if (kampanyaliSatisFiyatiCell == null) {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
                else {
                    kampanyaliSatisFiyatiCell.setCellValue(kampanyaliSatisFiyati);
                }
            }
        }

        // http response sending
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            sheet.getWorkbook().write(bos); // this is modified workbook.
        } finally {
            bos.close();
        }

        byte[] modifiedExcel = bos.toByteArray();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "filename=\"THE FILE NAME\"");
        response.setContentLength(modifiedExcel.length);

        OutputStream os = response.getOutputStream();

        try {
            os.write(modifiedExcel , 0, modifiedExcel.length);
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            os.close();
        }
        System.out.println("Sending excel to client as a http response please download the response");
    }

}
