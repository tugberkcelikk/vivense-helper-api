package com.vivense.vivensehelperapi.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@RestController
@ApiIgnore
@Api(hidden = true)
public class FileController {

    @PostMapping("file")
    public void uploadFile(@RequestPart(required = true) MultipartFile file, HttpServletResponse response) throws IOException {

        String contentOfFile = new String(file.getBytes());


        System.out.println(file.getName());
        System.out.println("slam");

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "filename=\"THE FILE NAME\"");
        response.setContentLength("BABA".length());

        OutputStream os = response.getOutputStream();

        try {
            os.write("BABA".getBytes(), 0, "BABA".length());
        } catch (Exception excp) {
            excp.printStackTrace();
        } finally {
            os.close();
        }
        System.out.println("Sending excel to client as a http response please download the response");
    }
}
