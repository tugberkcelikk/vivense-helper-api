package com.vivense.vivensehelperapi.service;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
public class ExcelService {

    public static void main(String[] args) {
    }

    public String greet() {
        return "Hello, World";
    }

    public Workbook getExcelWorkbookFromHttpRequest(HttpServletRequest request) throws IOException {

        try {
            Workbook workbook = new XSSFWorkbook(request.getInputStream()); // request body as a input stream, byte []
            return workbook;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }



}
