package com.vivense.vivensehelperapi.util;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Scanner;

public class Util {

    // elinde requst var bodysinde birşey almaca burada string getInputStream içinde body byte array
    public static String extractPostRequestBody(HttpServletRequest request) {
        if ("GET".equalsIgnoreCase(request.getMethod())) {
            Scanner s = null;
            try {
                s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }
}
