package com.vivense.vivensehelperapi.util;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

public class ExcelUtil {

    public static Sheet sheetFromWorkbookByPosition(HttpServletRequest request, int positionOfSheet) {
        try {
            Workbook workbook = WorkbookFactory.create(request.getInputStream()); // request body as byte stream
            return workbook.getSheetAt(positionOfSheet);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Sheet sheetFromWorkbookByPositionMultiPartFile(MultipartFile file, int positionOfSheet) {
        try {
            Workbook workbook = WorkbookFactory.create(file.getInputStream());
            return workbook.getSheetAt(positionOfSheet);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
